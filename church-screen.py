#!usr/bin/env python3
"""Run the church slideshow and service on Sunday."""
import json
import logging
import pafy
import urllib
from datetime import datetime
from dbus.exceptions import DBusException
from omxplayer.player import OMXPlayer, OMXPlayerDeadError
from os import mkdir, path
from pynput.keyboard import Key, KeyCode, Listener
from time import sleep

PATH = '/home/pi/church-screen/'


class KeyboardInputs:
    """Holds information about which keys have been pressed."""

    def __init__(self):
        """Set it up."""
        self._inputs = set()
        self._aliases = {}
        self._listener = Listener(on_press=self.listener)
        self._listener.start()

    def __getitem__(self, arg):
        """Check a key from either a character or pynput structure."""
        if isinstance(arg, str):
            if len(arg) > 1:
                return any([self[k] for k in self._aliases[arg]])
            else:
                if KeyCode.from_char(arg) in self._inputs:
                    return True
        return arg in self._inputs

    def __setitem__(self, arg, new_val):
        """Set a pynput structure to True or False."""
        if isinstance(arg, str):
            if len(arg) > 1:
                for key in self._aliases[arg]:
                    self[key] = new_val
            else:
                if new_val:
                    self._inputs.add(KeyCode.from_char(arg))
                else:
                    self._inputs.discard(KeyCode.from_char(arg))
        else:
            if new_val:
                self._inputs.add(arg)
            else:
                self._inputs.discard(arg)

    def add_alias(self, alias, keys):
        """Add an alias connected to one or multiple keys."""
        self._aliases[alias] = keys

    def listener(self, key):
        """Ran by the pynput listener on key presses."""
        logger.info(f'Key Press: {key}')
        self[key] = True

    def handle(self):
        """Handle 'q', esc or space."""
        if self['quit']:
            raise KeyboardInterrupt
        elif self['pause']:
            video_player.toggle_pause()
            self['pause'] = False

    def quit(self):
        """Properly shuts down the pynput listener."""
        self._listener.stop()


class VideoPlayer:
    """Handles playing, pausing and handing over of videos."""

    def __init__(self):
        """Create the player and whether or not it is paused."""
        self._player = None
        self._video = None
        self._args = None
        self.paused = False
        self._finished = False

    def set_video(self, video, args=''):
        """End the previous video and start the new one."""
        self.stop()
        logger.info(f'Playing: {video}')
        self._video = video
        self._args = args
        self._player = OMXPlayer(video, f'{args} --no-keys')
        self.paused = False

    def toggle_pause(self):
        """Toggle the video paused or un-paused."""
        if self._player is not None:
            if self.paused:
                self._player.play()
            else:
                self._player.pause()
            self.paused = not self.paused

    def _player_stopped(self, *_):
        logger.info('Player Stopped')
        self._finished = True

    def join(self, handler=lambda: None):
        """Join the video and so wait until it ends."""
        self._finished = False
        self._player.exitEvent += self._player_stopped
        while True:
            try:
                if self._finished:
                    logger.info('Video Ended')
                    break
                if handler() is False:
                    logger.info('Handler Raised')
                    self.stop()
                    break
                sleep(DELAY)
            except (DBusException, OMXPlayerDeadError):
                logger.exception('Something Failed: Restarting')
                self.set_video(self._video, self._args)
        self._player = None

    def stop(self):
        """Stop the player if it is running."""
        if self._player is not None:
            self._player.quit()
            self._player = None


def check_time(start_time=(9, 55), end_time=(10, 30)):
    """Check if the time is within the given bounds on a Sunday."""
    global played
    now = datetime.now()
    if not played:
        if now.weekday() == 6:
            if (
                (now.hour > start_time[0] or
                    (now.hour == start_time[0] and
                        now.minute >= start_time[1])) and
                (now.hour < end_time[0] or
                    (now.hour == end_time[0] and now.minute <= end_time[1]))
            ):
                played = True
                return True
    else:
        if (
            now.weekday() != 6 or
            now.hour > end_time[0] or
            (now.hour == end_time[0] and now.minute > end_time[1])
        ):
            played = False
    return False


def get_service(url=None, timeout=-1):
    """Get the service link or a video with the given url."""
    if url is None:
        url = urllib.request.urlopen(
            'https://www.portswood.org/this-sunday').url
    logger.info(f'Service url: {url}')
    timeout_counter = 0
    while True:
        try:
            best = pafy.new(url).getbest()
            break
        except OSError:
            inputs.handle()
            if timeout_counter == timeout:
                logger.warning('Timed out waiting for link to go live')
                return (None, None)
            timeout_counter += 1
            sleep(DELAY)
    return best.url


try:
    if not path.isdir(f'{PATH}logs'):
        mkdir(f'{PATH}logs')
    logging.basicConfig(
        filename=f'{PATH}logs/{datetime.now():%Y%m%dT%H%M%S}.log',
        filemode='w',
        format='%(levelname)s %(asctime)s - %(message)s',
        level=logging.INFO
    )
    logger = logging.getLogger()

    played = False
    video_player = VideoPlayer()
    inputs = KeyboardInputs()

    with open(f'{PATH}screen-config.json') as fp:
        config = json.load(fp)
        DELAY = config['delay']
        SLIDES = PATH + config['slides']
        for alias, keys in config['key_commands'].items():
            for i, key in enumerate(keys):
                keys[i] = Key.esc if key == 'esc' else keys[i]
                keys[i] = Key.space if key == 'space' else keys[i]
            inputs.add_alias(alias, keys)

    video_player.set_video(SLIDES, '--loop --no-osd')
    while True:
        if (check_time(config['start_time'], config['end_time'])
                or inputs['service']):
            url = get_service(timeout=900)  # 15 minute timeout
            inputs['service'] = False
            if url is None:
                continue
            video_player.set_video(url, '--no-osd')

            def _service_key_handler():
                inputs.handle()
                if inputs['service']:
                    inputs['service'] = False
                    return False
            video_player.join(_service_key_handler)
            video_player.set_video(SLIDES, '--loop --no-osd')
        if inputs['sound_test']:
            video_player.set_video('/usr/share/sounds/alsa/Front_Center.wav')
            inputs['sound_test'] = False
            video_player.join()
            video_player.set_video(SLIDES, '--loop --no-osd')
        inputs.handle()
        sleep(DELAY)
except:  # noqa
    logger.exception('')
    raise
finally:
    logger.warning('Program Finished')
    video_player.stop()
    inputs.quit()
