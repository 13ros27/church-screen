# Church Screen

To run power up the TV and the pi should powerup, will autoplay at 9:55.
Key Commands:
- p -> Play Service (at other times)/Play Slideshow
- Space -> Pause/Unpause
- t -> Sound Test
- q / Esc -> Exit program, generally don't need this, just power it off

Basic Update:
- Esc
- Ctrl+Alt+T
- git pull
- sudo reboot

To add to a pi:
- Esc
- Ctrl+Alt+T
- git clone https://gitlab.com/13ros27/church-screen.git
- sudo cp church-screen/autostart /etc/xdg/lxsession/LXDE-pi
- sudo cp church-screen/wpa_supplicant.conf /etc/wpa-supplicant
- sudo reboot

To update a pi:
- Esc
- Ctrl+Alt+T
- cd church-screen
- git pull
- Depending on what has changed:
	- sudo cp autostart /etc/xdg/lxsession/LXDE-pi
	- sudo cp wpa_supplicant.conf /etc/wpa-supplicant
- sudo reboot
